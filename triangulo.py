
import sys

def line(number: int):
    """Return a string corresponding to the line for number"""

    linea = number * str(number)
    return int(linea)

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""

    for x in range(1,number + 1):
        tr = 0
        tr2 = 0

        if x == number:
            lis = line(x)
            tr2 = tr2 + lis
            return tr2

        else:
            lis = line(x)
            tr = tr + lis
            print(tr)

def main():
    try:
        ###number = int(input("Dame un numero:"))
        number = int(sys.argv[1])
        if (0 < number < 10):
            text = triangle(int(number))
            print(text)
        else:
            raise ValueError
    except ValueError:
        print("Ha de ser un numero menor que 9 y mayor que 0")
        ###return main()

if __name__ == '__main__':
    main()
